package ufba.br.treinamento.persistence;

import ufba.br.treinamento.domain.Aluno;
import br.gov.frameworkdemoiselle.stereotype.PersistenceController;
import br.gov.frameworkdemoiselle.template.JPACrud;

@PersistenceController
public class AlunoDAO extends JPACrud<Aluno, Long> {

	private static final long serialVersionUID = 1L;

	public Aluno retornaNome(){
		return new Aluno();
	}
	
}