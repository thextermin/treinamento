package ufba.br.treinamento.business;

import ufba.br.treinamento.domain.Aluno;
import ufba.br.treinamento.persistence.AlunoDAO;
import br.gov.frameworkdemoiselle.stereotype.BusinessController;
import br.gov.frameworkdemoiselle.template.DelegateCrud;

@BusinessController
public class AlunoBC extends DelegateCrud<Aluno, Long, AlunoDAO> {

	
	
	private static final long serialVersionUID = 1L;
	
	public Aluno retornaNomeBC(){
		
		Aluno aluno = getDelegate().retornaNome();
		
		aluno.setNome("joao");
				
		return aluno;
	}
	
}
