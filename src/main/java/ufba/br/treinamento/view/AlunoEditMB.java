package ufba.br.treinamento.view;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import ufba.br.treinamento.business.AlunoBC;
import ufba.br.treinamento.domain.Aluno;
import br.gov.frameworkdemoiselle.annotation.PreviousView;
import br.gov.frameworkdemoiselle.stereotype.ViewController;
import br.gov.frameworkdemoiselle.template.AbstractEditPageBean;
import br.gov.frameworkdemoiselle.transaction.Transactional;

@ViewController
@PreviousView("/aluno_list.xhtml")
public class AlunoEditMB extends AbstractEditPageBean<Aluno, Long> {

	private static final long serialVersionUID = 1L;

	@Inject
	private AlunoBC alunoBC;
	
	@PostConstruct
	public void init(){
		
//		getBean().setNome("fernando");
		
	}
	
	public String retornaNomeBanco(){
		Aluno a = alunoBC.retornaNomeBC();
		
		getBean().setNome(a.getNome());
		
		return getPreviousView();
	}
	
	@Override
	@Transactional
	public String delete() {
		alunoBC.delete(getId());
		return getPreviousView();
	}

	@Override
	public String insert() {
		alunoBC.insert(getBean());
		return getPreviousView();
	}

	@Override
	public String update() {
		alunoBC.update(getBean());
		return getPreviousView();
	}

	@Override
	protected Aluno handleLoad(Long arg0) {
		
		return alunoBC.load(getId());
	}

	public AlunoBC getAlunoBC() {
		return alunoBC;
	}

	public void setAlunoBC(AlunoBC alunoBC) {
		this.alunoBC = alunoBC;
	}

}
