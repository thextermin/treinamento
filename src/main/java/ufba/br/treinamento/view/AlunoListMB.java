package ufba.br.treinamento.view;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import ufba.br.treinamento.business.AlunoBC;
import ufba.br.treinamento.domain.Aluno;
import br.gov.frameworkdemoiselle.annotation.NextView;
import br.gov.frameworkdemoiselle.annotation.PreviousView;
import br.gov.frameworkdemoiselle.stereotype.ViewController;
import br.gov.frameworkdemoiselle.template.AbstractListPageBean;
import br.gov.frameworkdemoiselle.transaction.Transactional;

@ViewController
@NextView("/aluno_edit.xhtml")
@PreviousView("/aluno_list.xhtml")
public class AlunoListMB extends AbstractListPageBean<Aluno, Long> {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private AlunoBC alunoBC;
	
	private List<Aluno> alunos;
	
	@Override
	protected List<Aluno> handleResultList() {
		alunos = alunoBC.findAll();
		return alunos;//alunoBC.findAll();
	}
	
public void atualizaGrid(){
		
		Random ran = new Random();
		
		Aluno aluno = new Aluno();
		aluno.setNascimento(Calendar.getInstance().getTime());
		aluno.setNome("Aluno de numero "+ran.nextInt(9));
		aluno.setTelefone("(71)8819-"+ran.nextInt(9999));
		
		alunoBC.insert(aluno);
		
		alunos.add(aluno);
		
	}
	
	@Transactional
	public String deleteSelection() {
		boolean delete;
		for (Iterator<Long> iter = getSelection().keySet().iterator(); iter.hasNext();) {
			Long id = iter.next();
			delete = getSelection().get(id);
			
			if (delete) {
				alunoBC.delete(id);
				iter.remove();
			}
		}
		return getPreviousView();
	}
	

	public AlunoBC getAlunoBC() {
		return alunoBC;
	}

	public void setAlunoBC(AlunoBC alunoBC) {
		this.alunoBC = alunoBC;
	}

	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

}
